#!/usr/bin/perl

# thinkfan-config - automatic thinkfan configuration
#
# Copyright 2017 Francesco Montanari <francesco.montanari@openmailbox.org>
#
# Automatically read sensors devices and set up accordingly thinkfan.conf
# from scratch. This script is supposed to be called by thinkfan.service.

use strict;
use warnings;
use File::Find::Rule;


# Header.
my @head = ("# Based on the thinkfan.conf.simple template\n\n");


# CPUs.
my $dir = '/sys/devices/platform/';

my @cpu = File::Find::Rule->file()
    ->name("temp*_input")
    ->in($dir);

for (@cpu)
{
    $_ = "hwmon " . $_ . "\n";
}


# Hard disks (not really necessary).
my @hd = ("atasmart /dev/sda\n");

my @temp = ("(0,	0,	55)\n",
            "(1,	48,	60)\n",
            "(2,	50,	61)\n",
            "(3,	52,	63)\n",
            "(4,	56,	65)\n",
            "(5,	59,	66)\n",
            "(7,	63,	32767)\n");


# Save configuration file.
my $strout = join('', @head,@cpu,@hd,@temp);
print ref($strout);

my $filename = '/etc/thinkfan.conf';
open(my $fh, '>', $filename) or die "Could not open file '$filename' $!";
print $fh $strout;
close $fh;
